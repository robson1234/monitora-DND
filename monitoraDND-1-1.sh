#!/bin/bash
#
# Descrição     : Shell script para monitorar agentes com DND ativo, o script deve ser executado de hora em hora
# Autor		: Robson Correa 
# Email		: rcleite1@stefanini.com
# 
#


# Declarando variaveis utilizadas no script
CAT=/bin/cat
GREP=/bin/grep
GAWK=/bin/gawk
ASTERISK=/usr/sbin/asterisk
UNIQ=/usr/bin/uniq
HEAD=/usr/bin/head
SED=/bin/sed
USER=root
PASS=*****senha*********
MYSQL=/usr/bin/mysql
TAIL=/usr/bin/tail
CUT=/bin/cut
DATE=/bin/date
HORA=`$DATE +"%H"`
HORA1=$(($HORA-1))
FILTRO="`$DATE +"%Y-%m-%d"` $HORA1"


#comando abaixo faz a leitura do log full da ultima hora e armazena a saída em log.txt
#$CAT /var/log/asterisk/full | $GREP -i "do not" | $GAWK -F" " '{print$15}' | $GAWK -F: '{print$1}' > log.txt
$CAT /var/log/asterisk/full | $GREP "$FILTRO" | $GREP -i "do not" | $GAWK -F" " '{print$1,$2,$3,$15}' > log.txt

#variável DNDATIVO recebe o conteudo do arquivo log.txt
DNDATIVO=`$CAT log.txt`

#verifica se há conteudo em DNDATIVO, se não houver não tem nenhum agente com dnd ativo
if [[ -z $DNDATIVO ]]; then	
	echo "sem DNDS ativos"

#define as variaveis que serão inseridas no banco de dados
else		
        LINHAS=`$CAT -n log.txt | $GAWK -F' ' '{print$1}' | $TAIL -n1 `
	for ((I=1; I<=	$LINHAS; I++ )); do
                #PESQUISA=`$SED -n 18p log.txt`
                #RAMAIS=`$ASTERISK -rx "sip show peers" | $GREP 10.42.26.13`
                PESQUISA=`$GAWK -F" " '{print$4}' log.txt | $GAWK -F: '{print$1}' | $SED -n $I'p'`
#		echo $PESQUISA
                RAMAIS=`$ASTERISK -rx "sip show peers" | $GREP "$PESQUISA" | $GAWK -F' ' '{print$1,$2}'`
#               echo $RAMAIS
		HORA=`$SED -n $I'p' log.txt | $GAWK -F] '{print$1}'`
#		echo $HORA
		HORAUID=`$SED -n $I'p' log.txt | $CUT -b22-24`
#		echo $HORAUID
		LIGAID=`$SED -n $I'p' log.txt | $CUT -b43-52`
#		echo $LIGAID
		UNIQID=`$CAT /var/log/asterisk/full | $GREP $LIGAID | $GREP 'UID' | $GAWK -F'"' '{print$4}'`	
#		echo $UNIQID
#Verifica se a variável $RAMAIS está vazia
		if [[ -z $RAMAIS ]]; then
			echo "vazio"
#armazena o valor das variaveis no banco de dados
		else
#			echo "teste"
#			echo "$HORA; $RAMAIS; $UNIQID"
			$MYSQL -u $USER -p$PASS -e "USE DNDATIVO; INSERT INTO ramaisdnd (hora, ramal, UID) VALUES ('$HORA', '$RAMAIS', '$UNIQID')"
		fi
#	sleep 1
    done

fi
